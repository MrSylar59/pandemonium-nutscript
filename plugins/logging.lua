PLUGIN.name = "Logging"
PLUGIN.author = "Black Tea"
PLUGIN.desc = "Vous pouvez modifier le système de log avec ce plugin."
 
if (SERVER) then
	local L = Format

	function PLUGIN:CharacterLoaded(id)
		local character = nut.char.loaded[id]
		local client = character:getPlayer()

		nut.log.add(client:steamName().." ("..client:SteamID()..") a chargé le personnage #"..id.." ("..character:getName()..")")
	end

	function PLUGIN:OnCharDelete(client, id)
		nut.log.add(client, "a supprimé le personnage #"..id, FLAG_WARNING)
	end

	function PLUGIN:PlayerDeath(victim, inflictor, attacker)
		if (victim:IsPlayer() and attacker) then
			if (attacker:IsWorld() or victim == attacker) then
				nut.log.add(victim, "est mort(e)")
			else
				local victimName = victim:Name().." ("..victim:SteamID()..")"

				if (attacker:IsPlayer()) then
					nut.log.add(attacker, L("a tué %s avec %s", victimName, inflictor:GetClass()))
				else
					nut.log.add(L("%s a tué %s avec %s.", tostring(attacker), victimName, inflictor:GetClass()))
				end
			end
		end
	end

	function PLUGIN:OnTakeShipmentItem(client, itemClass, amount)
		local itemTable = nut.item.list[itemClass]
		nut.log.add(client, L("a récupéré %s d'une caisse.", itemTable.name))
	end

	function PLUGIN:OnCreateShipment(client, shipmentEntity)
		nut.log.add(client, L("a fait une commande."))
	end

	function PLUGIN:OnCharTradeVendor(client, vendor, x, y, invID, price, isSell)
		local inventory = nut.item.inventories[invID]
		local itemTable = inventory:getItemAt(x, y)
		nut.log.add(client, L("%s %s.", isSell and "a vendu" or "a acheté", itemTable.name))
	end

	local logInteractions = {
		["drop"] = true,
		["take"] = true,
		["equip"] = true,
		["unequip"] = true,
	}

	function PLUGIN:OnPlayerInteractItem(client, action, item)
		if (logInteractions[action:lower()]) then
			if (type(item) == "Entity") then
				if (IsValid(item)) then
					local entity = item
					local itemID = item.nutItemID
					item = nut.item.instances[itemID]
				else
					return
				end
			elseif (type(item) == "number") then
				item = nut.item.instances[item]
			end

			if (!item) then
				return
			end

			nut.log.add(client, L("a utilisé \"%s\" sur %s.", action, item.name))
		end
	end
end