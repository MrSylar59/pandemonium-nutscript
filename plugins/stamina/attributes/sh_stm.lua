ATTRIBUTE.name = "Vigueur"
ATTRIBUTE.desc = "Modifie la vitesse à laquelle vous pouvez courir."

function ATTRIBUTE:onSetup(client, value)
	client:SetRunSpeed(nut.config.get("runSpeed") + value)
end