PLUGIN.name = "More Logging"
PLUGIN.author = "Black Tea"
PLUGIN.desc = "If you want to be american NSA, so be it."

if SERVER then
	function PLUGIN:OnItemTransfered( player, entity, itemTable )
		if (player:IsValid() and entity:IsValid() and itemTable) then
			nut.util.AddLog(Format("%s transféré dans un container par %s", itemTable.uniqueID, player:Name()), LOG_FILTER_ITEM)
		end
	end
	function PLUGIN:OnItemDropped( player, itemTable, entity )
		if (player:IsValid() and itemTable) then
			nut.util.AddLog(Format("%s a fait tomber %s.", player:Name(), itemTable.uniqueID), LOG_FILTER_ITEM)
		end
	end
	function PLUGIN:OnItemTaken( itemTable )
		if (itemTable.player:IsValid() and itemTable) then
			nut.util.AddLog(Format("%s a prit %s.", itemTable.player:Name(), itemTable.uniqueID), LOG_FILTER_ITEM)
		end
	end
	function PLUGIN:OnWeaponEquipped( player, itemTable, equip )
		local string = "enlève"
		if (equip) then 
			string = "équipe"
		end
		nut.util.AddLog(Format("%s %s %s.", itemTable.player:Name(),string,itemTable.uniqueID), LOG_FILTER_ITEM)
	end
	function PLUGIN:OnClothEquipped( player, itemTable, equip )
		local string = "enlève"
		if (equip) then 
			string = "équipe"
		end
		nut.util.AddLog(Format("%s %s %s.", itemTable.player:Name(),string,itemTable.uniqueID), LOG_FILTER_ITEM)
	end
	function PLUGIN:OnPartEquipped( player, itemTable, equip )
		local string = "enlève"
		if (equip) then 
			string = "équipe"
		end
		nut.util.AddLog(Format("%s %s %s.", itemTable.player:Name(),string,itemTable.uniqueID), LOG_FILTER_ITEM)
	end
end

