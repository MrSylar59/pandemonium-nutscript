PLUGIN.name = "Meilleur Polices d'Écriture"
PLUGIN.author = "MrSylar59"
PLUGIN.desc = "Ajoute des polices utilisables afin de rendre le chat plus lisible."

if (CLIENT) then
	surface.CreateFont("nutChatWhisper", {
		font = "Segoe UI",
		size = 15,
		weight = 1000,
		antialias = true
	})

	surface.CreateFont("nutChatYell", {
		font = "Segoe UI",
		size = 24,
		weight = 1000,
		antialias = true
	})
end