--[[
	PLUGIN.definitions[model's name(lowercase)] = {
		name = "Crate",
		desc = "A simple wooden create.",
		width = 4,
		height = 4,
		locksound = "",
		opensound = "",
	}
--]]

PLUGIN.definitions["models/props_junk/wood_crate001a.mdl"] = {
	name = "Caisse",
	desc = "Une simple caisse en bois.",
	width = 4,
	height = 4,
}

PLUGIN.definitions["models/props_c17/lockers001a.mdl"] = {
	name = "Casier",
	desc = "Un casier blanc.",
	width = 3,
	height = 5,
}

PLUGIN.definitions["models/props_wasteland/controlroom_storagecloset001a.mdl"] = {
	name = "Armoire Métalique",
	desc = "Une armoire métalique verte.",
	width = 4,
	height = 5,
}

PLUGIN.definitions["models/props_junk/trashbin01a.mdl"] = {
	name = "Petite Poubelle",
	desc = "Une petite poubelle de recyclage.",
	width = 2,
	height = 3,
}


PLUGIN.definitions["models/props_wasteland/controlroom_filecabinet002a.mdl"] = {
	name = "Armoire à Fichiers",
	desc = "Une armoire à fichiers verte.",
	width = 2,
	height = 4,
}

PLUGIN.definitions["models/items/ammocrate_smg1.mdl"] = {
	name = "Caisse de Munitions",
	desc = "Une lourde caisse qui stock des munitions.",
	width = 5,
	height = 3,
	onOpen = function(entity, activator)
		local seq = entity:LookupSequence("Close")
		entity:ResetSequence(seq)

		timer.Simple(2, function()
			if (entity and IsValid(entity)) then
				local seq = entity:LookupSequence("Open")
				entity:ResetSequence(seq)
			end
		end)
	end,
}

PLUGIN.definitions["models/props_c17/furnituredrawer001a.mdl"] = {
	name = "Commode en Bois",
	desc = "Une commode en bois lustré.",
	width = 4,
	height = 3,
}

PLUGIN.definitions["models/props_c17/furnituredrawer002a.mdl"] = {
	name = "Table de Chevet en Bois",
	desc = "Une table de chevet en bois lustré.",
	width = 1,
	height = 1,
}

PLUGIN.definitions["models/props_c17/furnituredrawer003a.mdl"] = {
	name = "Tiroir en Bois",
	desc = "Un tiroir en bois lustré.",
	width = 1,
	height = 4,
}

PLUGIN.definitions["models/props_c17/furnituredresser001a.mdl"] = {
	name = "Armoire en Bois",
	desc = "Une lourde armoire en bois lustré.",
	width = 4,
	height = 5,
}

PLUGIN.definitions["models/props_c17/furniturefridge001a.mdl"] = {
	name = "Réfrigérateur",
	desc = "Un vieux réfrigérateur, à peine fonctionnel.",
	width = 2,
	height = 3,
}

PLUGIN.definitions["models/props_junk/cardboard_box001a.mdl"] = {
	name = "Carton",
	desc = "Un carton en bon état.",
	width = 2,
	height = 2,
}

PLUGIN.definitions["models/props_junk/cardboard_box002a.mdl"] = {
	name = "Carton",
	desc = "Un vieux carton usagé.",
	width = 2,
	height = 2,
}

PLUGIN.definitions["models/props_junk/cardboard_box003a.mdl"] = {
	name = "Carton",
	desc = "Un carton postale.",
	width = 2,
	height = 2,
}

PLUGIN.definitions["models/props_junk/trashdumpster01a.mdl"] = {
	name = "Grande Poubelle",
	desc = "Une grande poubelle verte à odeur nauséabonde.",
	width = 4,
	height = 3,
}

PLUGIN.definitions["models/props_junk/wood_crate002a.mdl"] = {
	name = "Caisse Double",
	desc = "Une caisse double en bois.",
	width = 8,
	height = 4,
}

PLUGIN.definitions["models/props_lab/filecabinet02.mdl"] = {
	name = "Armoire à Firchiers",
	desc = "Une petite armoire à fichiers verte.",
	width = 2,
	height = 2,
}

PLUGIN.definitions["models/props_trainstation/trashcan_indoor001a.mdl"] = {
	name = "Poubelle de Rue",
	desc = "Une petite poubelle métalique verte.",
	width = 2,
	height = 3,
}

PLUGIN.definitions["models/props_wasteland/kitchen_fridge001a.mdl"] = {
	name = "Grand Réfrigérateur",
	desc = "Un grand réfrigérateur métalique extrêmement lourd.",
	width = 5,
	height = 6,
}