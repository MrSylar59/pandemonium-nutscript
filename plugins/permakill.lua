PLUGIN.name = "Permakill"
PLUGIN.author = "MrSylar59"
PLUGIN.desc = "Ajoute la possibilité de mort permanente."

function PLUGIN:PlayerDeath(client, inflictor, attacker)
	local character = client:getChar()

	if (nut.config.get("pkActive")) then
		if !(nut.config.get("pkWorld") and (client == attacker or inflictor:IsWorld())) then
			return
		end

		character:setData("banned", true)
	end
end

function PLUGIN:PlayerSpawn(client)
	local character = client:getChar()

	if (nut.config.get("pkActive") and character and character:getData("banned")) then
		character:kick()
	end
end

nut.config.add("pkActive", false, "Active le PK sur le serveur.", nil, {
	category = "Permakill"
})

nut.config.add("pkWorld", false, "Active le PK suite à un écrasement au sol.", nil, {
	category = "Permakill"
})