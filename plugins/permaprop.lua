PLUGIN.name = "Permaprop"
PLUGIN.author = "MrSylar59"
PLUGIN.desc = "Permet de rendre des props permanent même entre les cleanups."

local PLUGIN = PLUGIN

if (SERVER) then

	function PLUGIN:SavePermaProps(client, ent, perma) 
		local permaprops = nut.data.get("permaprops", {})
		local autoremove = nut.data.get("autoremove", {})

		if (perma) then
			for k, v in pairs(permaprops) do
				if (k == ent:GetCreationID() or v.uid == ent.uid) then
					return client:notifyLocalized("Vous tentez d'enregistrer un props déjà permanent.")
				end
			end

			local data = permaprops
			data[ent:GetCreationID()] = {
				pos = ent:GetPos(),
				ang = ent:GetAngles(),
				model = ent:GetModel(),
				uid = ent:GetCreationID()
			}

			nut.data.set("permaprops", data)
			client:notifyLocalized("PermaProp ajouté.")
		else
			local uid = (ent.uid or ent:GetCreationID())

			for k, v in pairs(permaprops) do
				if (k == uid) then
					local data = {}
					data[uid] = nil
					nut.data.set("permaprops", data)
				end
			end

			for k, v in pairs(autoremove) do
				if (v == ent:MapCreationID()) then
					return client:notifyLocalized("Vous tentez de faire supprimer un prop déjà marqué pour suppression.")
				end
			end

			if (ent:MapCreationID() == -1) then 
				ent:Remove()
				return client:notifyLocalized("AutoremoveProp ajouté.") 
			end

			local data = autoremove
			data[#data + 1] = ent:MapCreationID()
			ent:Remove()

			nut.data.set("autoremove", data)
			client:notifyLocalized("AutoremoveProp ajouté.")
		end
	end

	function PLUGIN:LoadData() 
		local permaprops = nut.data.get("permaprops", {})
		local autoremove = nut.data.get("autoremove", {})

		for k, v in pairs(autoremove) do
			for _, entity in pairs(ents.GetAll()) do
				if (v == entity:MapCreationID()) then
					entity:Remove()
				end
			end
		end

		for k, v in pairs(permaprops) do
			local entity = ents.Create("prop_physics")
			entity:SetPos(v.pos)
			entity:SetAngles(v.ang)
			entity:SetModel(v.model)
			entity:Spawn()
			entity.uid = v.uid

			local physObj = entity:GetPhysicsObject()
			if (IsValid(physObj)) then
				physObj:EnableMotion(false)
			end
		end
	end

end

nut.command.add("setpermaprop", {
	adminOnly = true,
	syntax = "[bool permanent]",
	onRun = function(client, arguments) 
		local permaProp = util.tobool(arguments[1])
		local data = {}
			data.start = client:GetShootPos()
			data.endpos = data.start + client:GetAimVector()*96
			data.filter = client
		local trace = util.TraceLine(data)
		local entity = trace.Entity

		if (!IsValid(entity) and entity:GetClass() != "prop_physics") then
			return client:notifyLocalized("Vous ne regardez pas un prop valide.")
		end

		if (permaProp) then
			PLUGIN:SavePermaProps(client, entity, true)
		else
			PLUGIN:SavePermaProps(client, entity, false)
		end
	end
})