-- You can change the default language here:
nut.config.language = "french"

--[[
	DO NOT CHANGE ANYTHING BELOW THIS.

	This is the NutScript main configuration file.
	This file DOES NOT set any configurations, instead it just prepares them.
	To set the configuration, there is a "Config" tab in the F1 menu for super admins and above.
	Use the menu to change the variables, not this file.
--]]

nut.config.add("maxChars", 5, "Le nombre maximal de personnage que peut avoir un joueur.", nil, {
	data = {min = 1, max = 50},
	category = "characters"
})
nut.config.add("color", Color(75, 119, 190), "La couleur principale du framework.", nil, {category = "appearance"})
nut.config.add("font", "Impact", "La police d'écriture de base.", function(oldValue, newValue)
	if (CLIENT) then
		hook.Run("LoadFonts", newValue)
	end
end, {category = "appearance"})
nut.config.add("maxAttribs", 100, "Le nombre maximal de points d'attributs autorisés.", nil, {
	data = {min = 1, max = 250},
	category = "characters"
})
nut.config.add("maxAttribsPoints", 30, "Le nombre maximal de points d'attributs à répartir à la création du personnage.", nil, {
	data = {min = 1, max = 250},
	category = "characters"
})
nut.config.add("chatRange", 280, "La distance maximale à laquelle le chat IC s'estompe.", nil, {
	form = "Float",
	data = {min = 10, max = 5000},
	category = "chat"
})
nut.config.add("chatColor", Color(255, 239, 150), "Couleur du chat par défaut.", nil, {category = "chat"})
nut.config.add("chatListenColor", Color(168, 240, 170), "Couleur du chat quand vous regardez votre interlocuteur.", nil, {category = "chat"})
nut.config.add("oocDelay", 10, "Délais en seconde avant qu'un joueur puisse envoyer un autre message en OOC.", nil, {
	data = {min = 0, max = 10000},
	category = "chat"
})
nut.config.add("loocDelay", 0, "Le délais en seconde avant qu'un joueur puisse envoyer un autre message en LOOC.", nil, {
	data = {min = 0, max = 10000},
	category = "chat"
})
nut.config.add("spawnTime", 5, "Le temps nécessaire à la réapparition.", nil, {
	data = {min = 0, max = 10000},
	category = "characters"
})
nut.config.add("invW", 6, "Nombre de solt dans une rangée d'inventaire.", nil, {
	data = {min = 0, max = 20},
	category = "characters"
})
nut.config.add("invH", 4, "Nombre de slot dans une colone d'inventaire.", nil, {
	data = {min = 0, max = 20},
	category = "characters"
})
nut.config.add("minDescLen", 16, "Nombre minimum de caractère que doit contenir une description.", nil, {
	data = {min = 0, max = 300},
	category = "characters"
})
nut.config.add("saveInterval", 300, "A quelle fréquence les personnages sont-ils sauvegardés.", nil, {
	data = {min = 60, max = 3600},
	category = "characters"
})
nut.config.add("walkSpeed", 130, "A quelle vitesse quelqu'un marche de manière ordinaire.", function(oldValue, newValue)
	for k, v in ipairs(player.GetAll())	do
		v:SetWalkSpeed(newValue)
	end
end, {
	data = {min = 75, max = 500},
	category = "characters"
})
nut.config.add("runSpeed", 235, "A quelle vitesse quelqu'un cours de manière ordinaire.", function(oldValue, newValue)
	for k, v in ipairs(player.GetAll())	do
		v:SetRunSpeed(newValue)
	end
end, {
	data = {min = 75, max = 500},
	category = "characters"
})
nut.config.add("walkRatio", 0.5, "Vitesse de marche quand on maintient ALT.", nil, {
	form = "Float",
	data = {min = 0, max = 1},
	category = "characters"
})
nut.config.add("punchStamina", 10, "Quantité de stamina consommée par un coup de poing.", nil, {
	data = {min = 0, max = 100},
	category = "characters"
})
nut.config.add("music", "music/hl2_song2.mp3", "La musique par défaut du jeu.", nil, {
	category = "appearance"
})
nut.config.add("logo", "http://nutscript.rocks/nutscript.png", "L'icone qui s'affiche dans le menu du jeu. Taille max: 86x86", nil, {
	category = "appearance"
})
nut.config.add("logoURL", "http://nutscript.rocks/", "La page qui est ouverte quand on clique sur l'icone.", nil, {
	category = "appearance"
})
nut.config.add("sbRecog", false, "Si la reconnaissance est utilisée également dans le score board.", nil, {
	category = "characters"
})
nut.config.add("defMoney", 0, "Le taux d'argent avec lequel commence un joueur.", nil, {
	category = "characters",
	data = {min = 0, max = 1000}
})
nut.config.add("allowVoice", false, "Autoriser ou non le chat vocal.", nil, {
	category = "server"
})
nut.config.add("sbWidth", 0.325, "Largeur du scoreboard en pourcentage d'occupation de l'écran.", function(oldValue, newValue)
	if (CLIENT and IsValid(nut.gui.score)) then
		nut.gui.score:Remove()
	end
end, {
	form = "Float",
	category = "visual",
	data = {min = 0.2, max = 1}
})
nut.config.add("sbHeight", 0.825, "Hauteur du scoreboard en pourcentage d'occupation de l'écran.", function(oldValue, newValue)
	if (CLIENT and IsValid(nut.gui.score)) then
		nut.gui.score:Remove()
	end
end, {
	form = "Float",
	category = "visual",
	data = {min = 0.3, max = 1}
})
nut.config.add("wepAlwaysRaised", false, "Si les armes sont toujours levées.", nil, {
	category = "server"
})