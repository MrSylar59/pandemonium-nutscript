ITEM.name = "Du Soin"
ITEM.model = "models/healthvial.mdl"
ITEM.width = 1
ITEM.height = 1
ITEM.healthAmmount = 0
ITEM.desc = "De quoi se soigner et arrêter les hémorragies."
ITEM.category = "Médical"

local playerMeta = FindMetaTable("Player")

function playerMeta:PlayCsSound(sound) 
	if (CLIENT) then
		surface.PlaySound(sound)
	end
end

local function GetFormatedName(name) 
	local det = "La "
	if (name:lower() == "bandage") then
		det = "Le "
	end

	return det .. name:lower()
end

ITEM.functions.Use = {
	name = "Utiliser",
	tip = "Applique l'objet sur soi.",
	onRun = function(item) 
		local client = item.player
		
		if (client:Health() < client:GetMaxHealth() or client.bleeding == true) then
			if (item.healthAmmount > 0) then
				client:SetHealth(math.min(client:Health() + item.healthAmmount, client:GetMaxHealth()))
				client:PlayCsSound("items/medshot4.wav")
			end

			if (client.bleeding) then
				client.bleeding = nil
				client:notifyLocalized(GetFormatedName(item.name) .. " a stoppé votre hémorragie.")
			end
		else
			client:notifyLocalized("Vous n'avez pas besoin de soin pour le moment.")
			return false
		end
	end
}

ITEM.functions.Give = {
	name = "Donner",
	tip = "Applique l'objet sur une autre personne.",
	onRun = function(item) 
		local client = item.player
		local data = {}
			data.start = client:EyePos()
			data.endpos = data.start + client:GetAimVector()*96
			data.filter = client
		local target = util.TraceLine(data).Entity

		if ((IsValid(target) or IsValid(target.nutRagdoll)) and target:IsPlayer()) then
			if (target:Health() < target:GetMaxHealth() or target.bleeding) then
				if (target.bleeding) then
					target.bleeding = nil
					target:notifyLocalized(GetFormatedName(item.name) .. " de " .. client:Name() .. " a stoppé votre hémorragie.")
					client:notifyLocalized("Votre ".. item.name .." a stoppé l'hémorragie de "..target:Name()..".")
				end

				if (target:Health() < target:GetMaxHealth()) then
					target:SetHealth(math.min(target:Health() + item.healthAmmount, target:GetMaxHealth()))
					target:PlayCsSound("items/medshot4.wav")
					client:notifyLocalized("Vous avez donner votre "..item.name.." à "..target:Name()..".")
				end
			else
				client:notifyLocalized(target:Name().." n'a pas besoin de soin pour le moment.")

				return false
			end
		else
			client:notifyLocalized("Vous ne regardez personne de valide.")

			return false
		end
	end
}