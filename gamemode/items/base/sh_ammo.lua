ITEM.name = "Ammo Base"
ITEM.model = "models/Items/BoxSRounds.mdl"
ITEM.width = 1
ITEM.height = 1
ITEM.ammo = "pistol" // type of the ammo
ITEM.ammoAmount = 30 // amount of the ammo
ITEM.desc = "A Box that contains %s of Pistol Ammo"
ITEM.category = "Munition"

function ITEM:getDesc()
	return Format(self.desc, self.ammoAmount)
end

if (CLIENT) then
	function ITEM:paintOver(item, w, h)
		draw.SimpleText(item.ammoAmount, "DermaDefault", w - 3, h - 3, color_white, TEXT_ALIGN_RIGHT, TEXT_ALIGN_TOP, 1, color_black)
	end
end

local function GetWeapon(client) 
	local weapons = {}

	for k, v in pairs(client:getChar():getInv():getItems()) do
		if (v.isWeapon and v:getData("equip")) then
			weapons[#weapons+1] = v
		end
	end

	if (weapons[1] != nil) then
		return weapons
	else
		return
	end
end

// On player uneqipped the item, Removes a weapon from the player and keep the ammo in the item.
ITEM.functions.use = { -- sorry, for name order.
	name = "Recharger",
	tip = "useTip",
	icon = "icon16/add.png",
	onRun = function(item)
		local client = item.player
		local weapons = GetWeapon(client)

		if (weapons) then
			client:GiveAmmo(item.ammoAmount, item.ammo)
			client:EmitSound("items/ammo_pickup.wav", 110)
		else
			client:notifyLocalized("Vous devez avoir une arme pour recharger.")

			return false
		end
	end,
}
