ITEM.name = "Weapon"
ITEM.desc = "A Weapon."
ITEM.category = "Armes"
ITEM.model = "models/weapons/w_pistol.mdl"
ITEM.class = "weapon_pistol"
ITEM.width = 2
ITEM.height = 2
ITEM.isWeapon = true
ITEM.weaponCategory = "sidearm"

-- Inventory drawing
if (CLIENT) then
	function ITEM:paintOver(item, w, h)
		if (item:getData("equip")) then
			surface.SetDrawColor(110, 255, 110, 100)
			surface.DrawRect(w - 14, h - 14, 8, 8)
		end
	end
end

-- On item is dropped, Remove a weapon from the player and keep the ammo in the item.
ITEM:hook("drop", function(item)
	if (item:getData("equip")) then
		item:setData("equip", nil)

		item.player.carryWeapons = item.player.carryWeapons or {}

		local weapon = item.player.carryWeapons[item.weaponCategory]

		if (IsValid(weapon)) then
			item:setData("ammo", item.player:GetAmmoCount(weapon:GetPrimaryAmmoType()) + weapon:Clip1())

			item.player:StripWeapon(item.class)
			item.player.carryWeapons[item.weaponCategory] = nil
			item.player:EmitSound("items/ammo_pickup.wav", 80)
		end
	end
end)

-- On player uneqipped the item, Removes a weapon from the player and keep the ammo in the item.
ITEM.functions.EquipUn = { -- sorry, for name order.
	name = "Ranger",
	tip = "equipTip",
	icon = "icon16/cross.png",
	onRun = function(item)
		item.player.carryWeapons = item.player.carryWeapons or {}

		local weapon = item.player.carryWeapons[item.weaponCategory]

		if (!weapon or !IsValid(weapon)) then
			weapon = item.player:GetWeapon(item.class)	
		end

		if (weapon and weapon:IsValid()) then
			item:setData("ammo", item.player:GetAmmoCount(weapon:GetPrimaryAmmoType()) + weapon:Clip1())
		
			item.player:StripWeapon(item.class)
		else
			print(Format("[Nutscript] L'arme %s n'existe pas !", item.class))
		end

		item.player:EmitSound("items/ammo_pickup.wav", 80)
		item.player.carryWeapons[item.weaponCategory] = nil

		item:setData("equip", nil)
		
		return false
	end,
	onCanRun = function(item)
		return (!IsValid(item.entity) and item:getData("equip") == true)
	end
}

-- On player eqipped the item, Gives a weapon to player and load the ammo data from the item.
ITEM.functions.Equip = {
	name = "Équiper",
	tip = "equipTip",
	icon = "icon16/tick.png",
	onRun = function(item)
		local client = item.player
		local items = client:getChar():getInv():getItems()

		client.carryWeapons = client.carryWeapons or {}

		for k, v in pairs(items) do
			if (v.id != item.id) then
				local itemTable = nut.item.instances[v.id]

				if (itemTable.isWeapon and client.carryWeapons[item.weaponCategory] and itemTable:getData("equip")) then
					client:notify("Vous utilisez déjà ce type d'arme.")

					return false
				end
			end
		end
		
		if (client:HasWeapon(item.class)) then
			client:StripWeapon(item.class)
		end

		local weapon = client:Give(item.class)

		if (IsValid(weapon)) then
			client.carryWeapons[item.weaponCategory] = weapon
			client:SelectWeapon(weapon:GetClass())
			client:SetActiveWeapon(weapon)
			client:EmitSound("items/ammo_pickup.wav", 80)

			-- Remove default given ammo.
			if (client:GetAmmoCount(weapon:GetPrimaryAmmoType()) != weapon:Clip1() and item:getData("ammo", 0) == 0) then
				weapon:SetClip1(0)
			end

			item:setData("equip", true)

			client:SetAmmo(item:getData("ammo", 0) - weapon:Clip1(), weapon:GetPrimaryAmmoType())
		else
			print(Format("[Nutscript] L'arme %s n'existe pas !", item.class))
		end

		return false
	end,
	onCanRun = function(item)
		return (!IsValid(item.entity) and item:getData("equip") != true)
	end
}

local function GetAmmo(ammo) 
	if (ammo == 5) then return "357ammo"
	elseif (ammo == 1) then return "ar2ammo"
	elseif (ammo == 6) then return "crossbowammo"
	elseif (ammo == 3) then return "pistolammo"
	elseif (ammo == 8) then return "rocketammo"
	elseif (ammo == 7) then return "shotgunammo"
	elseif (ammo == 4) then return "smg1ammo"
	end
end

ITEM.functions.Empty = {
	name = "Vider",
	tip = "Vide une arme de ses munitions",
	icon = "icon16/coins_delete.png",
	onRun = function(item) 
		local client = item.player
		local weapon = item.player.carryWeapons[item.weaponCategory]
		local ammoPerBox = nut.item.list[GetAmmo(weapon:GetPrimaryAmmoType())].ammoAmount
		local ammoBox = math.floor(item:getData("ammo", 0) / ammoPerBox)

		if (!weapon and !IsValid(weapon)) then
			weapon = item.player:GetWeapon(item.class)
		end

		if (weapon and IsValid(weapon)) then
			for i = 1, ammoBox do
				nut.item.spawn(GetAmmo(weapon:GetPrimaryAmmoType()), client:getItemDropPos() + i*Vector(0, 0, 10))
			end

			item:setData("ammo", 0)
			client:EmitSound("items/ammo_pickup.wav", 80)
			client:SetAmmo(0, weapon:GetPrimaryAmmoType())
			weapon:SetClip1(0)
		end

		return false
	end,
	onCanRun = function(item) 
		local weapon = item.player:GetWeapon(item.class)

		if (weapon and IsValid(weapon)) then
			return (!IsValid(item.entity) and item:getData("equip") == true and (item:getData("ammo", 0) > 0 or item.player:GetAmmoCount(weapon:GetPrimaryAmmoType()) > 0))
		else
			return false
		end
	end
}

function ITEM:onCanBeTransfered(oldInventory, newInventory)
	if (newInventory and self:getData("equip")) then
		return newInventory:getID() == 0
	end

	return true
end

function ITEM:onLoadout()
	if (self:getData("equip")) then
		local client = self.player
		client.carryWeapons = client.carryWeapons or {}

		local weapon = client:Give(self.class)

		if (IsValid(weapon)) then
			client.carryWeapons[self.weaponCategory] = weapon
			client:SetAmmo(self:getData("ammo", 0), weapon:GetPrimaryAmmoType())
			weapon:SetClip1(0)
		else
			print(Format("[Nutscript] L'arme %s n'existe pas !", self.class))
		end
	end
end

function ITEM:onSave()
	local weapon = self.player:GetWeapon(self.class)

	if (IsValid(weapon)) then
		self:setData("ammo", self.player:GetAmmoCount(weapon:GetPrimaryAmmoType()) + weapon:Clip1())
	end
end

HOLSTER_DRAWINFO = {}

-- Called after the item is registered into the item tables.
function ITEM:onRegistered()
	if (self.holsterDrawInfo) then
		HOLSTER_DRAWINFO[self.class] = self.holsterDrawInfo
	end
end

hook.Add("PlayerDeath", "nutStripClip", function(client)
	client.carryWeapons = {}

	for k, v in pairs(client:getChar():getInv():getItems()) do
		if (v.isWeapon and v:getData("equip")) then
			nut.item.spawn(v.uniqueID, client:GetShootPos(), function(item)
				item:setData("ammo", v:getData("ammo", 0))
				v:remove()
			end, Angle())
		end
	end
end)